//
//  DashboardView.swift
//  VirusApp
//
//  Created by Jake Butterfield on 17/03/2021.
//

import SwiftUI
import Combine

struct PostMessage: Decodable {
    let status, message: String
}

class HttpPost: ObservableObject {
    
    @Published var confirm = false
    @Published var error = false
    @Published var setup = false
    
    var token = ""
    
    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        return formatter
    }
    
    func setToken(token: String) {
        self.token = token
        print("HttpPost Received Token")
    }
    
    func reset() {
        self.confirm = false
        self.error = false
    }
    
    func testResult(result: String, date: Date) {
        guard let url = URL(string:
            "http://54.36.163.167:8000/api/result/") else { return }

        let newDate = self.dateFormatter.string(from: date)
        let body: [String: String] = [ "token": token, "result": result, "date": newDate]
        let expectedStatus = "201"
        
        print("POSTing Test Result")
        postData(url: url, body: body, status: expectedStatus)
    }
    
    func registerBluetooth(uuid: String) {
        guard let url = URL(string:
            "http://54.36.163.167:8000/api/bluetooth/") else { return }

        let body: [String: String] = ["token": token, "uuid": uuid]
        let expectedStatus = "201"
        
        print("POSTing Bluetooth UUID")
        postData(url: url, body: body, status: expectedStatus)
    }
    
    func reportBreach(uuid: String) {
        guard let url = URL(string:
            "http://54.36.163.167:8000/api/breach/") else { return }

        let body: [String: String] = ["token": token, "uuid": uuid]
        let expectedStatus = "201"
        
        print("POSTing Breach UUID")
        postData(url: url, body: body, status: expectedStatus)
    }
    
    func postData(url: URL, body: [String: String], status: String) {
        let finalBody = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = finalBody
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.reset()
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
            
            let finalData = try! JSONDecoder().decode(PostMessage.self, from: data)
            
            print("Received Status: " + finalData.status)
            
            if finalData.status == status {
                DispatchQueue.main.async {
                    self.confirm = true
                }
            } else {
                DispatchQueue.main.async {
                    self.error = true
                }
            }
            
        }.resume()
    }
}

struct DashboardView: View {
    
    var token: String
    
    @ObservedObject var postManager = HttpPost()
    @ObservedObject var bleManager = BLEManager()
    
    // Other Views
    @State private var visibleResult = false
    @State private var visibleAdvice = false
    
    var body: some View {
        
        ZStack {
            VStack(spacing: 16) {
                
                // Title
                Text("Contact Tracker")
                    .font(.system(size: 32, weight: .semibold))
                    .foregroundColor(.white)
                Spacer()
                
                if self.bleManager.scanning {
                    
                    Button(action: {
                        self.bleManager.StopScanning()
                    }){
                        Text("Stop Tracing")
                            .foregroundColor(.white)
                            .font(.system(size: 24, weight: .medium))
                    }.frame( maxWidth: .infinity )
                    .padding(.vertical, 20)
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(8.0)
                    .padding(.horizontal, 20)
                    
                } else {
                    
                    Button(action: {
                        self.bleManager.StartScanning()
                    }){
                        Text("Start Tracing")
                            .foregroundColor(.white)
                            .font(.system(size: 24, weight: .medium))
                    }.frame( maxWidth: .infinity )
                    .padding(.vertical, 20)
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(8.0)
                    .padding(.horizontal, 20)
                    
                }
                
                
                
                Text("To clock in, press the start button. You must keep this app running throughout our shift.")
                    .font(/*@START_MENU_TOKEN@*/.headline/*@END_MENU_TOKEN@*/)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
                Spacer()
                
                if bleManager.enabled {
                    Text("Bluetooth Enabled")
                        .foregroundColor(.green)
                } else {
                    Text("Bluetooth Disabled")
                        .foregroundColor(.red)
                }
                
                Button(action: {
                    visibleResult.toggle()
                }){
                    Text("Enter Test Result")
                        .foregroundColor(.white)
                        .font(.system(size: 24, weight: .medium))
                }.frame( maxWidth: .infinity )
                .padding(.vertical, 20)
                .background(Color.blue.opacity(0.8))
                .cornerRadius(8.0)
                .padding(.horizontal, 20)
                .fullScreenCover(isPresented: $visibleResult) {
                    ResultView( postManager: postManager )
                }
                
                Button(action: {
                    visibleAdvice.toggle()
                }){
                    Text("Latest Advice")
                        .foregroundColor(.white)
                        .font(.system(size: 24, weight: .medium))
                }.frame( maxWidth: .infinity )
                .padding(.vertical, 20)
                .background(Color.blue.opacity(0.8))
                .cornerRadius(8.0)
                .padding(.horizontal, 20)
                .sheet(isPresented: $visibleAdvice) {
                    AdviceView()
                }
                
            }
        }.onAppear {
            postManager.setToken(token: token)
            bleManager.setToken(token: token)
        }
        
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView( token: "" )
    }
}
