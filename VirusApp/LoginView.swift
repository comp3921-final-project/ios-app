//
//  LoginView.swift
//  VirusApp
//
//  Created by Jake Butterfield on 13/03/2021.
//
import SwiftUI

struct LoginView: View {
    @State var username: String = ""
    @State var password: String = ""
    
    @ObservedObject var authManager: HttpAuth
    
    var body: some View {
        VStack(spacing: 16) {
            Spacer()
            
            Text("Virus App")
                .font(.system(size: 64, weight: .semibold))
                .foregroundColor(.white)
            
            if self.authManager.errorOccured {
                Text("Incorrect Credentials! Please Try Again")
                    .font(.system(size: 16, weight: .semibold))
                    .foregroundColor(.white)
            }
            
            Text("Username:")
            TextField("Username", text: $username)
                .padding(.all, 20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal, 20)
                .foregroundColor(.black)
            
            Text("Password:")
            SecureField("Password", text: $password)
                .padding(.all, 20)
                .background(Color.white)
                .cornerRadius(8.0)
                .padding(.horizontal, 20)
                .foregroundColor(.black)
                .keyboardType(.numberPad)
            
            Button(action: {
                self.authManager.checkDetails(username: self.username, password: self.password)
            }) {
                Text("Login")
                    .foregroundColor(.white)
                    .font(.system(size: 24, weight: .medium))
            }.frame( maxWidth: .infinity )
            .padding(.vertical, 20)
            .background(Color.blue.opacity(0.8))
            .cornerRadius(8.0)
            .padding(.horizontal, 20)
            
            Spacer()
            Spacer()
        }.background(
            Image("virus")
                .resizable()
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
        ).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}
