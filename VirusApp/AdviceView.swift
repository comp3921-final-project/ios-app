//
//  AdviceView.swift
//  VirusApp
//
//  Created by Jake Butterfield on 24/03/2021.
//

import SwiftUI

struct AdviceView: View {
    var body: some View {
        WebView(url: "https://www.nhs.uk/conditions/coronavirus-covid-19/")
    }
}

struct AdviceView_Previews: PreviewProvider {
    static var previews: some View {
        AdviceView()
    }
}
