//
//  BLEManager.swift
//  VirusApp
//
//  Created by Jake Butterfield on 19/03/2021.
//

import Foundation
import CoreBluetooth
import UIKit

struct Peripheral: Identifiable {
    let id: Int
    let name: String
    let rssi: Int
    let uuid: String
}

class BLEManager: NSObject, ObservableObject, CBCentralManagerDelegate {
    
    @Published var enabled = true
    @Published var peripherals = [Peripheral]()
    @Published var scanning = false
    
    var token = ""
    
    var myUuid = ""
    var potential = [String: Int]()
    
    var myCentral: CBCentralManager!
    
    var postManager = HttpPost()
    
    func setToken(token: String) {
        self.token = token
        postManager.setToken(token: token)
        print("BLE Received Token")
    }
    
    func StartScanning() {
        print("Started Scanning")
        myCentral.scanForPeripherals(withServices: nil, options: nil)
        scanning = true
        myUuid = UIDevice.current.identifierForVendor!.uuidString
        print("Own UUID: " + myUuid)
        
        postManager.registerBluetooth(uuid: myUuid)
    }
    
    func StopScanning() {
        print("Stopped Scanning")
        myCentral.stopScan()
        scanning = false
    }
    
    func LogBreach(breachID: String) {
        print("BREACH!")
        print(myUuid + " / " + breachID)
        
        postManager.reportBreach(uuid: breachID)
    }
    
    func LogPotentialBreach( uuid: String ) {
        let s = Int(Date().timeIntervalSince1970)
        
        for( u, t ) in potential {
            if u == uuid {
                if (s - t) > 120 {
                    // If 2 mins have passed, update time
                    potential[u] = s;
                }
                else if (s - t) > 60 {
                    // If 60 seconds have passed, log breach
                    LogBreach(breachID: u)
                    potential[u] = nil
                }
            }
        }
        
        let keyExists = potential[uuid] != nil
        if !keyExists {
            potential[uuid] = s
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        var peripheralName: String!
        
        if let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
            peripheralName = name
        } else {
            peripheralName = "Unknown"
        }
        
        let deviceID = peripheral.identifier
        let deviceUUIDString = NSString(string: deviceID.uuidString)
        let uuid: String = deviceUUIDString as String
        
        let newPeripheral = Peripheral(id: peripherals.count, name: peripheralName, rssi: RSSI.intValue, uuid: uuid)
        
        if (newPeripheral.rssi > -60) {
            LogPotentialBreach(uuid: uuid)
        }
        
        //print(newPeripheral)
        //print(advertisementData)
        
        peripherals.append( newPeripheral )
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            enabled = true
        } else {
            enabled = false
        }
    }
    
    override init() {
        super.init()
        myCentral = CBCentralManager(delegate: self, queue: nil)
        myCentral.delegate = self
    }
}
