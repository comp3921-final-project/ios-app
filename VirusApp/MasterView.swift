//
//  MasterView.swift
//  VirusApp
//
//  Created by Jake Butterfield on 19/03/2021.
//

import SwiftUI
import Combine

struct AuthMessage: Decodable {
    let status, message, token: String
}

class HttpAuth: ObservableObject {
    
    @Published var authed = false
    @Published var token = ""
    @Published var errorOccured = false
    
    func checkDetails(username: String, password: String) {
        guard let url = URL(string:
            "http://54.36.163.167:8000/api/login/") else { return }
        
        let body: [String: String] = ["username": username, "passcode": password]
        
        let finalBody = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = finalBody
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else { return }
            
            let finalData = try! JSONDecoder().decode(AuthMessage.self, from: data)
            
            if finalData.status == "200" {
                
                DispatchQueue.main.async {
                    self.token = finalData.token
                    self.authed = true
                }
                
            } else {
                
                print(finalData.message)
                
                DispatchQueue.main.async {
                    self.errorOccured = true
                }
            }
            
        }.resume()
    }
}

struct MasterView: View {
    
    @ObservedObject var authManager = HttpAuth()
    
    var body: some View {
        
        if self.authManager.authed {
            let token = self.authManager.token
            DashboardView( token: token )
        } else {
            LoginView( authManager: authManager )
        }
    }
}

struct MasterView_Previews: PreviewProvider {
    static var previews: some View {
        MasterView()
    }
}
