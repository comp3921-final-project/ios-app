//
//  ResultView.swift
//  VirusApp
//
//  Created by Jake Butterfield on 24/03/2021.
//

import SwiftUI

struct ResultView: View {
    
    @ObservedObject var postManager: HttpPost
    
    @Environment(\.presentationMode) var presentationMode
    
    @State private var selectedResult = "Negative"
    let results = ["Negative", "Positive"]
    
    @State private var testDate = Date()
    
    var body: some View {
        
        NavigationView {
            Form {
                Section {
                    DatePicker(selection: $testDate, in:...Date(), displayedComponents: .date) {
                        Text("Test Date")
                    }
                    
                    Text("Test Result:")
                    Picker("Test Result", selection: $selectedResult) {
                        ForEach(results, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(WheelPickerStyle())
                    
                    Button(action: {
                        self.postManager.testResult(result: selectedResult, date: testDate)
                        presentationMode.wrappedValue.dismiss()
                    }){
                        Text("Submit Test Result")
                            .foregroundColor(.white)
                            .font(.system(size: 18, weight: .medium))
                    }.frame( maxWidth: .infinity )
                    .padding(.vertical, 15)
                    .background(Color.blue.opacity(0.8))
                    .cornerRadius(8.0)
                }
            }
            .navigationTitle("Report a Test Result")
        }
        
        Spacer()
        
        Button(action: {
            presentationMode.wrappedValue.dismiss()
        }){
            Text("Cancel")
                .foregroundColor(.white)
                .font(.system(size: 20, weight: .medium))
        }.frame( maxWidth: .infinity )
        .padding(.vertical, 20)
        .background(Color.red.opacity(0.8))
        .cornerRadius(8.0)
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView( postManager: HttpPost() )
    }
}
